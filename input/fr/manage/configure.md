# Configurer votre Projet

![tip](warning "Cette manière de configurer PSDK sera remplacée par des fichiers .json dans Data")

## Le Script Config

Dans l'Editeur de Scripts RMXP, vous trouverez un script appelé `Config`. Il contient tout ce qu'il faut pour configuerer votre projet (incluant des constantes importantes).

![Config|center](img/manage/config.png "Le Script Config")

Voici la liste des principales constantes et les valeurs qu'elles doivent contenir :
- `Title` : C'est le titre de votre jeu, il devrait être de type `String`.
- `ScreenWidth` : C'est la largeur native de l'écran, qui devrait être un Integer. La largeur de l'écran est cette valeur multipliée par ScreenScale.
- `ScreenHeight` : C'est la hauteur native de l'écran, qui devrait être un Integer. La hauteur de l'écran est cette valeur multipliée par ScreenScale.
- `CenterPlayer` : C'est un booléen qui permet au joueur de toujours être au centre de l'écran. Mettez la valeur à `false` pour autoriser la caméra à s'arrêter quand le joueur arrive en bord de map et que le MapLinker est désactivé.
- `Pokemon_Max_Level` : C'est le niveau max par défaut (pour le calcul de l'exp). Ce doit être un nombre supérieur ou égal à `$pokemon_party.level_max_limit`.  
    ![tip](info "Si vous voulez limiter le niveau des Pokémons du joueur, utilisez `$pokemon_party.level_max_limit = max_level` dans une commande script dans les bons évènements (Evènement de départ & de Champions d'Arène lorsqu'ils donnent les badges).")
- `DisableMouse` : Attribuez `true` à cette valeur pour désactiver la souris dans votre jeu.

## La Configuration des Viewports

Si vous retirer le module `Viewport` du script `Config`, PSDK utilisera `Data/Viewport.json` comme source de configuration des viewports.

Le fichier ressemble à ça :
```{
  "main": {
    "x": 0,
    "y": 0,
    "width": 320,
    "height": 240
  }
}```

C'est basiquement un Hash de Hash qui contient toutes les configurations de viewport. Quand vous écrivez :
```ruby
  @viewport = Viewport.create(:main, z)
```
Les paramètres x, y, width & height sont déterminés dans ce fichier. Si vous voulez créer des viewports qui ne font pas l'intégralité de l'écran, vous pouvez utiliser ce fichier de configuration.

![tip](info "`Viewport.create(type, z)` est le seul moyen recommander pour créer un Viewport. Il permet au viewport d'être déplacé quand vous souhaitez ajouter une compatibilité avec le plein écran grâce à `Viewport::GLOBAL_OFFSET_X` & `Viewport::GLOBAL_OFFSET_Y`. N'oubliez pas que PSDK est en 4:3 par défaut et que la plupart des écrans d'ordinateurs sont en 16:9 or 16:10.")