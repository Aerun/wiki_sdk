# Gérer PSDK

Dans cette section, nous parlerons de ce qui peut vous aider à gérer votre projet PSDK.

## Liste des articles

- [Installer PSDK](install.md)
- [Compiler sous Linux](build-under-linux.md)
- [Configurer votre projet](configure.md)
- [Installer un Script](install-script.md)