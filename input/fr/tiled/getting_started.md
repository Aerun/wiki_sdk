# Bien commencer

## Paramétrer un Nouveau Projet

Quand vous ouvrez tiled pour la première fois, vous atterrissez sur cette fenêtre :

![Première ouverture de Tiled](img/tiled/tiled_premiere_ouverture_full.png)

Depuis la mise à jour 1.4 de **Tiled**, il comprend un gestionnaire de projet qui vous permet de gérer vos maps et tilesets de manière logique et plus organisée. Pour que tous vos éléments soient accessibles depuis la fenêtre `Projet`, et donc plus facilement accessibles dans l'arborescence durant l'édition d'une map ou d'un tileset, il est recommandé de paramétrer un nouveau projet.

Sélectionnez `Projet`, `Sauver le Projet en tant que...`, puis placez le fichier à la racine de votre projet.

Vous pouvez alors ajouter des dossiers à votre projet pour organiser les choses :
- Un dossier `tilesets` pour y ranger vos tilesets.
- Un dossier `maps` pour y ranger vos maps.
- Un dossier `templates` pour y ranger vos templates.
- Etc.

![tip](info "Pour aller plus loin sur ce sujet, n'hésitez pas à vous référer à la [section correspondante](https://doc.mapeditor.org/en/stable/manual/projects/) de la Documentation de Tiled.")

## Créer une Nouvelle Carte

Pour créer une Nouvelle Carte, cliquez dans le menu contextuel sur `Fichier`, `Nouveau`, `Nouvelle Carte...`, ou appuyez simplement sur `CTRL + N`. Une fenêtre s'ouvre :

![Nouvelle Carte - Tiled](img/tiled/tiled_nouvelle_carte.png)

Vous pouvez alors configurer divers paramètres :
- La **taille de la carte** (un aperçu vous donne la conversion en pixels).
- La **taille des tuiles** (ou tiles).
- **L'orientation** (isométrique ou orthogonale). Pour du Pokémon, choisissez `Orthogonale`
- Le **format de calque**, compressé ou non. Choisissez `Base64 (compressé par zlib)`.
- L'**ordre d'affichage des tuiles**. Choisissez `En Haut à Gauche`.

![tip](info "Pas de panique, vous pourrez toujours modifier ces paramètres plus tard via `Carte` dans le menu contextuel et `Propriétés de la Carte...`.")

![tip](info "Si vous avez créé un projet Tiled, pensez bien à mettre votre carte dans le un dossier qui est géré par ce projet.")

## Créer un Nouveau Tileset

Une fois votre carte créée, il vous faut maintenant lui assigner des Jeux de Tuiles (Tilesets) pour pouvoir commencer à la réaliser. Sélectionnez `Fichier` dans le menu contextuel, `Nouveau`, `Nouveau Jeu de Tuiles...`. Une fenêtre s'ouvre :

![Nouveau Jeu de Tuiles - Tiled](img/tiled/tiled_nouveau_tileset.png)

Vous pouvez alors configurer divers paramètres :
- Le **nom** de votre tileset.
- La possibilité d'**embarquer le tileset** dans la carte directement. *Ce qui permet d'éviter de devoir mettre à jour les tilesets des maps à chaque modification de ces derniers.*
- Via le bouton `Rechercher` ou `ALT + B`, sélectionnez l'**image du tileset** à utiliser.
- L'utilisation d'une **couleur de transparence**. *Vous pouvez ne pas cocher cette case si vous n'utilisez pas de couleur de transparence.*
- La **taille des tuiles**. *32x32 dans notre cas*.
- Une **marge et un espacement**, qui permet d'ajuster le tileset par rapport à l'image. Si votre image a une marge de 20 pixels, mettez 20px. De la même façon, si un espace existe entre vos tiles sur l'image, c'est ici qu'il faut l'indiquer.

## Et maintenant ?

Vous pouvez alors **paramétrer votre Tileset**, y ajouter des terrains, modifier ses propriétés, etc. Une fois que c'est fait, revenez sur l'onglet de votre Carte. Vous voyez que le tileset précédemment créé apparait dans la liste des Jeux de Tuiles :

![Vos Jeu de Tuiles - Tiled](img/tiled/tiled_jeux_de_tuiles.png)

Vous pouvez alors cliquer sur l'outil `Tampon`, premier de la liste, sélectionner une tuile de votre jeu de tuiles, et commencer à placer vos tuiles sur la Carte.  
Amusez-vous bien !

![tip](info "Pensez à bien prendre le temps d'apprendre à utiliser les différents outils à votre disposition, et à paramétrer vos [calques](https://doc.mapeditor.org/en/stable/manual/layers/). Souvenez-vous que la documentation est toujours là si vous avez une question.")