# Système de Temps

PSDK a un système de temps pouvant fonctionner de deux manières différentes :
- En utilisant l'**Horloge Réelle** : Comme dans les jeux offiiciels, le temps s'écoule en fonction du temps de la console (dans le cas présent, votre ordinateur)
- En utilisant l'**Horloge Virtuelle** : Le temps est plus court (à moins que vous ne changiez ça) et s'écoule en fonction de l'horloge interne au jeu. Cela permet généralement au joueur de jouer à différents moment de la journée dans une même session de jeu et le temps reprend lorsque vous chargez votre sauvegarde.

Le Système de Temps est activé par défaut et est utilisé par les baies. L'horloge en jeu par défaut est l'horloge virtuelle, mais vous pouvez changer sur l'horloge réelle en utilisant cet interrupteur :

![Setting Real Time|center](https://i.imgur.com/fVMZytp.png "Changement de l'heure virtuelle pour l'heure réelle en utilisant l'interrupteur 7.")

Si vous voulez retourner à l'horloge virtuelle, désactivez juste cet interrupteur à nouveau.

![tip](info "Rappelez-vous qu'il est nécessaire que vous lanciez le script `Yuki::TJN.force_update_tone` pour mettre à jour le système de temps sur celui que vous utilisez actuellement.")

## Temps en jeu
Le jour en jeu actuel fonctionne comme présenté ici : 

| Matin | Journée  | Soirée | Nuit  |
| ------ | ------ | ------ | ------ |
| 07h00 - 10h59 | 11h00 - 18h59 | 19h00 - 21h59 | 22h00 - 06h59 |

Pour changer celui par défaut vous pouvez utiliser :
`Yuki::TJN::TIME_SETS[:default].clear.concat([21, 18, 11, 4])`

Ou vous pouvez ajouter votre set personnalisé:
`Yuki::TJN::TIME_SETS[:jaizu_dn] = [21, 18, 11, 4]`

![tip](warning "Si vous ajoutez votre propre set de temps, vous devrez aussi avoir votre propre système de ton. Pour changer sur votre système de temps et votre système de ton, utilisez `$pokemon_party.tint_time_set = :jaizu_dn`")

## Système de Ton

Le Système de Ton est activé par défaut dans le script d'introduction. Si vous utilisez le vôtre ou avez retiré cette partie, vous pouvez l'activer ou le désactiver en utilisant l'Interrupteur 10 : Système de Ton Actif.
Pour faire en sorte que le jeu mette à jour vos changements sans avoir besoin de vous téléporter ou rafraichir l'écran, appelez un script avec `Yuki::TJN.force_update_tone` dedans.

Le Système de Ton par défaut utilise 5 tons différents, comme cela :

```ruby
TONE_SETS = {
      default: [
        Tone.new(-85, -85, -20, 0), # Night
        Tone.new(-17, -51, -34, 0), # Evening
        Tone.new(-75, -75, -10, 0), # Midnight
        NEUTRAL_TONE, # Day
        Tone.new(17, -17, -34, 0) # Dawn
      ],
```
      
Il est aussi possible de définir 24 tons (un ton par heure) qui seront calculés chaque minute pour rendre la transition lisse :
`$pokemon_party.tint_time_set = :winter`

Les tons ne sont pas corrects, ils sont là pour servir de tests. Vous pouvez faire les vôtres en utilisant :

```ruby
Yuki::TJN::TONE_SETS[:jaizu_dn] = [
        Tone.new(-75, -75, -10, 0), # 0
        Tone.new(-80, -80, -10, 0), # 1
        Tone.new(-85, -85, -10, 0), # 2
        Tone.new(-80, -80, -12, 0), # 3
        Tone.new(-75, -75, -15, 0), # 4
        Tone.new(-65, -65, -18, 0), # 5
        Tone.new(-55, -55, -20, 0), # 6
        Tone.new(-25, -35, -22, 0), # 7
        Tone.new(-20, -25, -25, 0), # 8
        Tone.new(-15, -20, -30, 0), # 9
        Tone.new(-10, -17, -34, 0), # 10
        Tone.new(5, -8, -15, 0), # 11
        Tone.new(0, 0, -5, 0), # 12
        Tone.new(0, 0, 0, 0), # 13
        Tone.new(0, 0, 0, 0), # 14
        Tone.new(-10, -25, -10, 0), # 15
        Tone.new(-17, -51, -34, 0), # 16
        Tone.new(-20, -43, -30, 0), # 17
        Tone.new(-35, -35, -25, 0), # 18
        Tone.new(-45, -45, -20, 0), # 19
        Tone.new(-55, -55, -15, 0), # 20
        Tone.new(-60, -60, -14, 0), # 21
        Tone.new(-65, -65, -13, 0), # 22
        Tone.new(-70, -70, -10, 0), # 23
      ]
```

Rappelez-vous que le Système de Ton et les temps en jeu sont liés, donc si vous en ajoutez un personnalisé ils doivent avoir le même nom.
Vous pouvez aussi utiliser un système de ton simplifié comme ceci :
```ruby
Yuki::TJN::TONE_SETS[:jaizu_dn] = [
  night = Tone.new(-85, -85, -20, 0), # Night
  Tone.new(-17, -51, -34, 0), # Evening
  night, # Midnight
  Yuki::TJN::NEUTRAL_TONE, # Day
  Tone.new(17, -17, -34, 0) #Morning
]
```

## Manipuler le Temps
PSDK sauvegarde les valeurs de temps dans des variables. Pour manipuler le temps en jeu, changez les valeurs de ces variables :
- 0010: Heure (de 0 à 23)
- 0011: Minute (de 0 à 59)
- 0013: Jour de la semaine (de 0 à 6)
- 0014: Semaine (de 0 à X)
- 0015: Mois (de 0 à 11)
- 0016: Jour (de 0 à 364)

Pour rafraichir l'écran afin de changer le ton à celui de l'heure voulue, lancez un script avec  `Yuki::TJN.force_update_tone`

## Événements basés sur le temps réel
PSDK a des interrupteurs pour rendre les choses plus faciles :

![alt text](https://i.imgur.com/N0MfKDy.png "Liste des interrupteurs de temps")

Avec ceux-ci vous pouvez facilement créer des événements qui n'auront lieu qu'à certains moments de la journée.

![alt text](https://i.imgur.com/IcTaSSG.png "Événement utilisant un interrupteur de temps")

![tip](warning "Assurez-vous de ne pas trop utiliser ce genre d'événement, étant donné que ce n'est pas optimum et légèrement consommateur pour le processeur.")