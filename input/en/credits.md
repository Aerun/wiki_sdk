# Credits

## Engine related credits

### Pokémon Universe

Game Freak, CREATURES INC. et The Pokémon Company

### LiteRGSS

#### *Backend*

SFML

#### *Contributors*

Scorbutics  
nthoang-apcs  
SuperFola  
Nuri Yuri

### API

**FIRELIGHT TECHNOLOGIES PTY LTD.** : FMOD lowlevel API  
**[PokeAPI and its contributor](https://github.com/PokeAPI/pokeapi/blob/master/CONTRIBUTING.md#credits)** : Data input for newer versions

### Softwares

**Microsoft** : Visual Code

### Contributors

**Nuri Yuri** : Creator of PSDK  
**Aerun** : Active Contributor (DataBase, Moves Programming, UI Designer)  
**Rey** : Active Contributor (UI Programming)  
**SirMalo** : UI Designer  
**Leikt** : Old Contributor (Various System like PathFinding)

#### *PSDK Wiki*

Aerun  
Akiyra  
Bugfix  
buttjuice  
Guizmo  
Jaizu  
Metaiko  
Mud'  
Nuri Yuri  
ralandel  
Rey  
SMB64  
yyyyj  
Zenos

#### *DataBase*

AEliso  
Aerun  
Avori  
Bentoxx  
dragon935  
Eurons  
joeyw  
Mauduss  
Nuri Yuri  
Pαlвσlѕку  
PokeAPI  
RebelIce  
Tokeur  
Redder  
Schneitizel  
Walven

#### *Translation*

Aerun  
Dax  
Eduardo Dorantes  
Gehtdich Nichtsan  
Helio  
Jaizu  
Kaizer  
Leikt  
PadawanMoses  
Rey  
SirMalo  
SoloReprise  
SMB64  
yyyyj

#### *Ruby Host*

Aerun  
AEliso  
Bentoxx  
Buttjuice  
Maxoumi  
Nuri Yuri  
Splifingald  
yyyyj

#### *PSDK Editor*

Aerun  
Dax  
Mud'  
Nuri Yuri  
Walven  
Waris

#### *Other Contributors*

Amras  
Anti-NT  
Aye  
BigOrN0t  
Bouzouw  
Cayrac  
Diaband.onceron  
Fafa  
Jarakaa  
Kiros97  
Likton  
MrSuperluigis  
oldu49  
Otaku  
Shamoke  
Solfay  
sylvaintr  
UltimaShayra  
Unbreakables  

## Resource related credits

### Tilesets

#### *Epic Adventures tileset*
 
Alistair  
Alucus  
Bakura155  
Bati'  
Blue Beedrill  
BoOmxBiG  
Chimcharsfireworkd  
CNickC/CNC  
CrimsonTakai  
Cuddlesthefatcat  
Dewitty  
EpicDay  
Fused  
Gigatom  
Heavy-Metal-Lover  
Hek-el-grande  
Kage-No-Sensei  
Kizemaru_Kurunosuke  
Kyledove  
LaPampa  
LotusKing  
New-titeuf  
Novus  
Pokemon_Diamond  
PrinceLegendario  
Reck  
Red-Gyrados  
REMY35  
Saurav  
SL249  
Spaceemotion  
SirMalo  
Stefpoke  
sylver1984  
ThatsSoWitty  
TheEnglishKiwi  
Thegreatblaid  
ThePokemonChronicles  
TwentyOne  
UltimoSpriter  
Warpras  
WesleyFG  
Yoh  
Nuri Yuri  
07harris/Paranoid  
19dante91  
27alexmad27

### Overworlds Sprites

#### *Gen1 to 5 Overworlds*

2and2makes5  
Aerun  
Chocosrawlooid  
cSc-A7X  
Fernandojl  
Gallanty  
Getsuei-H  
Gizamimi-Pichu  
help-14  
kdiamo11  
Kid1513  
Kyle-Dove  
Kyt666  
Milomilotic11  
MissingLukey  
Pokegirl4ever  
Silver-Skies  
Syledude  
TyranitarDark  
Zyon17  
Zenos
    
#### *Gen6 Overworlds*

Princess-Phoenix  
LunarDusk6

### Pokémon Sprites

#### *Gen6 Pokémon Battlers (including megas)*

Amras Anarion  
BladeRed  
Diegotoon20  
Domino99designs  
Falgaia  
GeoisEvil  
Juan-Amador  
N-Kin  
Noscium  
SirAquaKip  
Smogon XY Sprite Project  
Zermonious  
Zerudez

#### *Gen7 Pokémon Battlers (including Alolan)*

Alex  
Amethyst  
Bazaro  
conyjams  
DatLopunnyTho  
Falgaia  
fishbowlsoul90  
Jan  
kaji atsu  
Koyo  
Leparagon  
Lord-Myre  
LuigiPlayer  
N-kin  
Noscium  
Pikafan2000  
princess-phoenix  
Smeargletail  
Smogon  
The cynical poet  
Zumi  

#### *Gen8 Pokémon Battlers*

WolfPP  
conyjams  

#### *Pokémon Offsets*

Don  

### Icon Sprites

#### *Animated Pokémon icons*

Pikachumazzinga  

#### *Gen7 Pokémon icons*

Otaku  
Poképedia  

#### *Item icons*

Maxoumi (Brick icon & ADN Berserk)
yyyyj (Pink Ribon icons)

### Texts

#### *Official Pokémon X/Y Texts*

Kaphotics  
Peterko  
Smogon  
X-Act

#### *Official Sun & Moon Texts*

Asia81  

### UI Design

#### *Alpha Ruins puzzle*

FL0RENT_

#### *Default Message Windowskin*

ralandel

#### *Key Binding UI*

Eurons

#### *Language Selection UI*

Mud'

#### *Options UI*

Mud'

#### *Quests UI*

Renkys

#### *Shop & Pokémon Shop UI*

Aerun

#### *Trainer Card*

Eurons

#### *XBOX 360 keys*

Yumekua

### Animations

#### *Gen6 Battle Entry**

Jayzon

#### *PSP Animations*

Metaiko  
Isomir (Close Combat)  
KLNOTHINCOMIN (Aeroblast, Energy Ball & Seed Flare)

#### *Shiny Animations*

Neslug

### Miscellaneous

#### *Battle Backs*

Midnitez-REMIX

## Special Thanks

PSDK would never have happend without **`Krosk`** the former creator of `Pokémon Script Project` which introduced Nuri Yuri to Pokémon Fangame Making in *2008*.
