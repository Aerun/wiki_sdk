# Getting Started

## Setting up a New Project

When launching Tiled for the first time, we are greeted with the following window:

![Première ouverture de Tiled](img/tiled/tiled_premiere_ouverture_full_en.png)

Since **Tiled** 1.4, you can manage maps and tilesets in a project to organize it. To make all our assets readily accessible from the `Project` view, as well as to be able to quickly switch between multiple projects, it is recommended to first set up a Tiled project. This is however an entirely optional step that can be skipped when desired.

Choose `Project`, `Save Project As…` to save a new project file. The recommended location is the root of your project.

You can add folders to your project to organize it:

- A `tilesets` folder to store tilesets.
- A `maps` folder to store maps.
- A `templates` folder to store templates.
- Etc.

![tip](info "To go further on this subject, please refer to the [corresponding section](https://doc.mapeditor.org/en/stable/manual/projects/) of the Tiled documentation.")

## Creating a New Map

To create a New Map, choose `File`, `New`, `New Map...`, or just push `CTRL + N`. The following dialog will pop up:

![New Map - Tiled](img/tiled/tiled_nouvelle_carte_en.png)

You can now configure various parameters:
- **Map size** (with a pixel size preview).
- **Tiles size**.
- Map's **orientation** (isometric or orthogonal). Choose `Orthogonal`.
- **Tile layer format**, compressed or not. Choose `Base64 (zlib compressed)`.
- **Tiled render order**, chose `Left Top`.

![tip](info "All of these things can be changed later by chosing `Map` then `Map Properties...`, so it’s not important to get it all right the first time.")

![tip](info "If you set up a project, make sure to save the map to a folder that you had added to your project.")

## Creating a New Tileset

After saving our map, we’ll see the tile grid and an initial tile layer will be added to the map. However, before we can start using any tiles we need to add a tileset. Choose `File`, `New` then `New Tileset…` to open the New Tileset dialog:

![New tileset - Tiled](img/tiled/tiled_nouveau_tileset_en.png)

You can now configure various parameters:
- Tileset's **name**.

- Le **nom** de votre tileset.
- We leave the **Embed in map** option disabled. This is recommended, since it will allow the tileset to be used by multiple maps without setting up its parameters again. It will also be good to store the tileset in its own file if you later add tile properties, terrain definitions, collision shapes, etc., since that information is then shared between all your maps.
- With `Browse...` button or `ALT + B`, choose the **tileset's picture** to use.
- You can use a **transparency color**. *Do not check the option if you don't use transparency color.*
- **Tiles' size**. *32x32 for PSDK*.
- It could be add a **pixel margin** around the tiles and a **pixel spacing** between the tiles (this is pretty rare actually, usually you should leave these values on 0).

## And now ?

You can now **set up your Tileset**, add terrains, edit properties, etc. Once it's done, go back to your map. You see your Tileset now appears in the Tilesets' list:

![Tilesets - Tiled](img/tiled/tiled_jeux_de_tuiles_en.png)

You can choose `Stamp Brush` tool, first of its list, then start painting using tiles. Enjoy it!

![tip](info "Take time to learn to use each tools you have on Tiled, and to set up your [layers](https://doc.mapeditor.org/en/stable/manual/layers/). Remember you have a documentation if you have a question.")