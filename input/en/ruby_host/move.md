# Move editor

![tip](warning "This page comes from the **old Wiki**. It's probably not up to date.")

## Move editor in Ruby Host
The `Moves` button in the Home UI opens the Moves Editor. The move behaviors depends on the method used (`:s_basic` for tackle).

![Move Editor UI|center](img/ruby_host/Rubyhost_skillmain.png "Move editor UI")

## Create a new move

1. Make sure all the texts for the move exists (Name + Description)
2. Click on `Add move`.
3. Set the move properties (watch how other move where done if needed)
5. If the move is a TM/HM that can trigger something from the Party Menu, set the ID of the common event to call in `Common event ID (map) :`
7. Choose the `Method used in BattleEngine:`.
    - `:s_basic` for tackle
    - `:s_stat` for Growl
    - `:s_status` for Will-O-Wisp
    - `:s_multi_hit` for Bullet Seed
    - `:s_2hits` for Double Hit
    - `:s_ohko` for Fissure
    - `:s_2turns` for Fly
    - `:s_self_stat` for Close Combat
    - `:s_self_statut` for a move that inflict status to the user.
